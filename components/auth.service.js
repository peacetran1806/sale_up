const API_URL = "https://api.saleup.com.au/graphql";


const login = (username, password) => {
  return fetch(API_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `mutation tokenAuth{
                tokenAuth(username:"`+ username + `", password:"` + password + `"){
                  token
                payload
                refreshExpiresIn
              }
            }`}),
  })
    .then((r) => {
      return r.json();
    })
    .then((response) => {
      if (response.data.tokenAuth.token) {
        localStorage.setItem("userToken", JSON.stringify(response.data))
      }
      return response.data.tokenAuth.token;
    });
}

export const logout = () => {
  localStorage.removeItem("userToken");
}

export const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem('userToken'));;
}


export default login;