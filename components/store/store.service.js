//import authHeader from '../auth-header';
//import fetchHelper from '../../helper/fetch.helper'


// export const getAllStore = () =>{
//   return fetch('https://api.saleup.com.au/graphql', {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': 'JWT ' + JSON.parse(localStorage.getItem("userToken")).tokenAuth.token
//     },
//     body: JSON.stringify({
//       query: `query stores{
//                     stores{
//                       name
//                       owner{
//                         username
//                         email
//                         firstName
//                         lastName
//                         phone
//                           }
//                       address
//                       phone
//                       website
//                       businessHours{
//                           weekDay
//                               isWorking
//                           openHour
//                           closeHour
//                       }
//                     }
//                   }`}),
//   })
//     .then((r) => {
//       return r.json();
//     })
//     .then((response) => {
//       return response;
//     });

// }

export const createStore = (query, variables) => {
  return fetch('https://api.saleup.com.au/graphql', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: query,
      variables: variables
    })
  })
    .then((r) => {
      return r.json()
    })
    .then((data) => {
      return data
    });
}


export default createStore;