import React, { Component } from 'react';
import Router from "next/router"
import Link from 'next/link'


function queryFetch(query,variables){
    fetch('https://api.saleup.com.au/graphql', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            query:query,
            variables:variables})
      })
        .then(res => res.json())
        .then(data => console.log({ data }));
}



class CreateStore extends Component{
    constructor(){
        super();
        this.state = {
            userid: '',
            password: '',
        };
        this.publish = this.publish.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }
    
    publish(){
        console.log(this.state.userid, this.state.password, this.state.storeName);
    }

    handleSubmit(){
        queryFetch(`mutation createStore($userid:String!, $password: String!, $storeName:String!){
            createStore(userid:$email, password:$password, storeName:$storeName){
              status
              msg
            }
            }`,{userid: this.state.userid, password: this.state.password, storeName: this.state.storeName}) ;
    }


    render(){
        return (
            <div>
                <div className="header">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="logo">
                                <svg>
                                    <line x1="0" y1="100%" x2="100%" y2="0" />
                                    <line x1="0" y1="0" x2="100%" y2="100%" />
                                </svg>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <label className="signupText" style={{ float: 'right' }}>Already have a SaleUp account?</label>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="left text-center">
                        <h1 style={{ paddingLeft: '1.5em', paddingRight: '1.5em', paddingBottom: '0.5em' }}>Start your 90-day free trial today</h1>
                        <p className="text-center" style={{ paddingLeft: '4.5em', paddingRight: '4.5em', paddingBottom: '1em' }}>Try shoify for free, and explore all the tools and services you need to start, run, and grow your business</p>
                        <form action="#" method="post" data-toggle="validator">
                            <div className="form-group">
                                <input name='userid' type="email" className="form-control" placeholder="Email address"  value={this.state.userid} onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <input name='password' type="password" className="form-control"  placeholder="Password"  value={this.state.password} onChange={this.handleChange}/>
                            </div>
                            <div className="form-group">
                                <button className="btn btn-default btn-block" onClick={this.handleSubmit} >
                                    <Link href="/MyAccount/MyAccount_Index">Login</Link>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div className="clear"></div>
                </div>
    
            </div>
        );
    }

}
export default CreateStore;
