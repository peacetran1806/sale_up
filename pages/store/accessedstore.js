export default function AccessedStore(){
    return (
      <div>
        <div className="container box">
          <div className="as-center">
            <div className="row">
              <div className="col-lg-4 col-md-4">
                <div className="logo as-logo">
                  <svg>
                    <line x1="0" y1="100%" x2="100%" y2="0" />
                    <line x1="0" y1="0" x2="100%" y2="100%" />
                  </svg>
                </div>
              </div>
            </div>
            <div
              className="row"
              style={{ paddingTop: ".5em", paddingBottom: ".5em" }}
            >
              <div className="col-lg-12 col-md-12">
                <h2>Recent accessed stores</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-10 col-md-10">
                <div style={{ display: "inline-flex" }}>
                  <span className="as-profileImg">JP</span>
                  <h4 style={{ marginTop: "5px", paddingLeft: "10px" }}>
                    Johannes.phathehai{"@"}gmail.com
                  </h4>
                </div>
              </div>
              <div className="col-lg-2 col-md-2">
                <p>logout</p>
              </div>
            </div>
            {/* stores */}
            <div className="row">
              <div className="col-lg-12 col-md-12">
                <div class="panel panel-default as-panel">
                  <div class="panel-body">
                      <div className="pull-left">
                        <p><b>district8-vn.com.vn</b></p>
                        <p>district8-vn.mysaleup.com</p>
                      </div>
                      <div className="pull-right">
                      <span class="glyphicon glyphicon glyphicon-chevron-right as-middleIcon" aria-hidden="true"></span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 col-md-12">
                <div class="panel panel-default as-panel">
                  <div class="panel-body">
                      <div className="pull-left">
                        <p><b>district8-vn.com.vn</b></p>
                        <p>district8-vn.mysaleup.com</p>
                      </div>
                      <div className="pull-right">
                      <span class="glyphicon glyphicon glyphicon-chevron-right as-middleIcon" aria-hidden="true"></span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 col-md-12">
                <div class="panel panel-default as-panel">
                  <div class="panel-body">
                      <div className="pull-left">
                        <p><b>district8-vn.com.vn</b></p>
                        <p>district8-vn.mysaleup.com</p>
                      </div>
                      <div className="pull-right">
                      <span class="glyphicon glyphicon glyphicon-chevron-right as-middleIcon" aria-hidden="true"></span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 col-md-12">
                <div class="panel panel-default as-panel">
                  <div class="panel-body">
                      <div className="pull-left">
                        <p><b>district8-vn.com.vn</b></p>
                        <p>district8-vn.mysaleup.com</p>
                      </div>
                      <div className="pull-right">
                      <span class="glyphicon glyphicon glyphicon-chevron-right as-middleIcon" aria-hidden="true"></span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 col-md-12">
                <div class="panel panel-default as-panel">
                  <div class="panel-body">
                      <div className="pull-left">
                        <p><b>district8-vn.com.vn</b></p>
                        <p>district8-vn.mysaleup.com</p>
                      </div>
                      <div className="pull-right">
                      <span class="glyphicon glyphicon glyphicon-chevron-right as-middleIcon" aria-hidden="true"></span>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            {/* buttons */}
            <div className="row text-center">
                 <div className="col-lg-12 col-md-12">
                    <button type="button" class="btn btn-link">Create new store</button>
                 </div>
            </div>
            <div className="row text-center">
                 <div className="col-lg-12 col-md-12">
                    <button type="button" class="btn btn-link">Log in to another Shopify ID</button>
                 </div>
            </div>
          </div>
        </div>
      </div>
    );
}