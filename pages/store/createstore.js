import React, { Component } from 'react';
import Router from "next/router"
import createStore from '../../components/store/store.service'

class CreateStore extends Component{
    constructor(){
        super();
        this.state = {
            emailAddress: '',
            password: '',
            storeName: ''
        };
        this.publish = this.publish.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }
    
    publish(){
        console.log(this.state.emailAddress, this.state.password, this.state.storeName);
    }

    handleSubmit(e){
        e.preventDefault();
        createStore(`mutation createStore($email:String!, $password: String!, $storeName:String!){
            createStore(email:$email, password:$password, storeName:$storeName){
              status
              msg
            }
            }`,{email: this.state.emailAddress, password: this.state.password, storeName: this.state.storeName}). then((response) =>{
                console.log(response);
                if(response.data.createStore.status === true){
                    alert("Created successfully");
                    Router.push("/MyAccount/MyAccount_Index");
                }
                else{
                    alert(response.data.createStore.msg);
                }
            });
            
    }


    submitHandler = e => {
        e.preventDefault();
        axios.post('',this.state);
    }
    render(){
        return (
            <div>
                <div className="header">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="logo">
                                <svg>
                                    <line x1="0" y1="100%" x2="100%" y2="0" />
                                    <line x1="0" y1="0" x2="100%" y2="100%" />
                                </svg>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <label className="signupText" style={{ float: 'right' }}>Already have a SaleUp account?</label>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="left text-center">
                        <h1 style={{ paddingLeft: '1.5em', paddingRight: '1.5em', paddingBottom: '0.5em' }}>Start your 90-day free trial today</h1>
                        <p className="text-center" style={{ paddingLeft: '4.5em', paddingRight: '4.5em', paddingBottom: '1em' }}>Try shoify for free, and explore all the tools and services you need to start, run, and grow your business</p>
                        <form action="#" method="post" data-toggle="validator" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <input name='emailAddress' type="email" className="form-control" placeholder="Email address"  value={this.state.emailAddress} onChange={this.handleChange} required/>
                            </div>
                            <div className="form-group">
                                <input name='password' type="password" className="form-control"  placeholder="Password"  value={this.state.password} onChange={this.handleChange} required/>
                            </div>
                            <div className="form-group">
                                <input name='storeName' type="text" className="form-control"  placeholder="Your store name"  value={this.state.storeName} onChange={this.handleChange} required/>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-default btn-block">Create your store</button>
                            </div>
                        </form>
                    </div>
                    <div className="right">
                        <p className="p1">You're in good company<br /><br />Over 1,000,000 stores, in more than 175 countries, truest Shopify to run their business.</p>
                        <svg>
                            <line x1="0" y1="100%" x2="100%" y2="0" />
                            <line x1="0" y1="0" x2="100%" y2="100%" />
                        </svg>
                    </div>
                    <div className="clear"></div>
                </div>
    
            </div>
        );
    }

}
export default CreateStore;
