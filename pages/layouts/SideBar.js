import Link from "next/link";
import {logout, getCurrentUser} from '../../components/auth.service';
import Router from 'next/router';
import { useRouter } from "next/router";

const handleLogout = () => () => {
    logout();
    if (getCurrentUser() === null) {
        Router.push("/login");
    }
};

const Sidebar = () => {
    const router = useRouter();
    return (
        <aside className="main-sidebar" style={{ display: 'table', backgroundColor:'#F5F6F9' }}>   
            <section className="sidebar">
                <ul className="sidebar-menu" data-widget="tree">
                    <li className={router.pathname == "/" ? "active" : ""}>
                        <Link href="/">
                            <a>
                                <i className="fa fa-dashboard"></i>
                                <span>Dashboard</span>
                            </a>
                        </Link>
                    </li>
                    <li className={router.pathname == "/clients/clientSreen" ? "active" : ""}>
                        <Link href="/clients/clientSreen">
                            <a>
                                <i className="fa fa-user-o"></i>
                                <span>Client</span>
                            </a>
                        </Link>
                    </li>
                    <li className="treeview">
                        <a href="#">
                            <i className="fa fa-credit-card"></i><span>Gift card</span>
                        </a>
                    </li>
                    <li className="treeview">
                        <a href="#">
                            <i className="fa fa-commenting-o"></i>
                            <span>Communication</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><a href="pages/layout/top-nav.html"><i className="fa fa-circle-o"></i> Top Navigation</a></li>
                            <li><a href="pages/layout/boxed.html"><i className="fa fa-circle-o"></i> Boxed</a></li>
                            <li><a href="pages/layout/fixed.html"><i className="fa fa-circle-o"></i> Fixed</a></li>
                            <li><a href="pages/layout/collapsed-sidebar.html"><i className="fa fa-circle-o"></i> Collapsed Sidebar</a>
                            </li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <a href="#">
                            <i className="fa fa-bar-chart"></i>
                            <span>Report</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><a href="pages/charts/chartjs.html"><i className="fa fa-circle-o"></i> ChartJS</a></li>
                            <li><a href="pages/charts/morris.html"><i className="fa fa-circle-o"></i> Morris</a></li>
                            <li><a href="pages/charts/flot.html"><i className="fa fa-circle-o"></i> Flot</a></li>
                            <li><a href="pages/charts/inline.html"><i className="fa fa-circle-o"></i> Inline charts</a></li>
                        </ul>
                    </li>
                    <li className="treeview">
                        <a href="#">
                            <i className="fa fa-cogs"></i>
                            <span>Setup</span>
                            <span className="pull-right-container">
                                <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><a href="pages/UI/general.html"><i className="fa fa-circle-o"></i> General</a></li>
                            <li><a href="pages/UI/icons.html"><i className="fa fa-circle-o"></i> Icons</a></li>
                            <li><a href="pages/UI/buttons.html"><i className="fa fa-circle-o"></i> Buttons</a></li>
                            <li><a href="pages/UI/sliders.html"><i className="fa fa-circle-o"></i> Sliders</a></li>
                            <li><a href="pages/UI/timeline.html"><i className="fa fa-circle-o"></i> Timeline</a></li>
                            <li><a href="pages/UI/modals.html"><i className="fa fa-circle-o"></i> Modals</a></li>
                        </ul>
                    </li>
                    <li className={router.pathname == "/myaccount/MyAccount_Index" ? "active" : ""}>
                        <Link href="/myaccount/MyAccount_Index">
                            <a>
                                <i className="fa fa-user-circle-o"></i>
                                <span>My Account</span>
                            </a>
                        </Link>

                    </li>
                </ul>
            </section>
            <div className="user-panel" style={{
                position: 'fixed',
                bottom: 0,
                width: '230px',
                paddingBottom: 0,
                borderTop: '1px solid black'
            }}>
                <div className="row mb-3">
                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        {/* <img src="dist/img/user-160x160.jpg" className="img-circle img-responsive" alt="User Image" /> */}
                        {/* <div style={{ borderRadius: '50%', width: '50px', height: '50px', border: '1px solid black' }}></div> */}
                    </div>
                    <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8 info">
                        <p style={{ marginBottom: '0' }}>Welcome</p>
                        <p style={{ marginBottom: '0' }}><b>Admin</b></p>
                    </div>
                </div>
                <div className="row">
                    <div className="btn-group btn-group-toggle btn-group-justified" data-toggle="buttons" style={{ width: '235px' }}>
                        <label className="btn btn-secondary active">
                            <input type="radio" name="options" id="option1" autoComplete="off" defaultChecked />
                            <i className="fa fa-question-circle-o"></i>
                        </label>
                        <label className="btn btn-secondary">
                            <input type="radio" name="options" id="option2" autoComplete="off" />
                            <i className="fa fa-search"></i>
                        </label>
                        <label className="btn btn-secondary">
                            <input type="radio" name="options" id="option3" autoComplete="off" onClick={handleLogout()} />
                            <i className="fa fa-sign-out"></i>
                        </label>
                    </div>
                </div>
                <div className="row">
                    <button type="button" className="btn btn-default btn-block">Invite a colleague</button>
                </div>
            </div>
        </aside>
    )

}

export default Sidebar;