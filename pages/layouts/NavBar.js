const NavBar = () => (
    <>
        <nav className="navbar navbar-static-top">
            <div className="row" style={{ width: '100%' }}>
                <button className="btn btn-default ml-3 mt-2" data-toggle="push-menu" role="button" style={{ backgroundColor: 'white', color: '#f44336', height: '40px', width: '40px' }}><span className="fa fa-bars"></span></button>
                {/* <a href="#" class="sidebar-toggle btn btn-default ml-3 mt-2" data-toggle="push-menu" role="button" style={{ backgroundColor: 'white', color: '#f44336', height: '40px', width: '40px' }}>
                <span className="fa fa-bars"></span>
            </a> */}
                <div className="col-9">

                    <div className="input-group mt-2 mb-3">
                        <div className="input-group-btn">
                            <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown">Action</button>
                            <ul className="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li className="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <input type="text" className="form-control" />
                    </div>
                </div>
                <div className="col-2">
                    <button className="btn btn-default mr-2 mt-2" data-toggle="push-menu" role="button" style={{ backgroundColor: 'white', color: '#fff' }}><span className="fa fa-bars"></span></button>
                    <button className="btn btn-default mr-2 mt-2" data-toggle="push-menu" role="button" style={{ backgroundColor: 'white', color: '#fff' }}><span className="fa fa-bars"></span></button>
                    <button className="btn btn-default mt-2" data-toggle="modal" data-target="#exampleModal" style={{ backgroundColor: 'white', color: '#fff' }}><span className="fa fa-bars"></span></button>
                </div>


            </div>
        </nav>
        <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-xl">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title" id="exampleModalLabel">Booking Title - <a href="#">Link</a></h4>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-7">
                                <div className="row">
                                    <div className="col-4">
                                        DOB select calendar
                                    </div>
                                    <div className="col-4">
                                        Gender
                                    </div>
                                    <div className="col-4">
                                        Client Group
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        Residential Address
                                    </div>
                                </div>
                            </div>
                            <div className="col-5">
                                <div className="row">
                                    <div className="col-12">
                                        Membership Level
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        Current Reward Point
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        Note
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Service</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Staff</th>
                                    <th scope="col">Rating</th>
                                    <th scope="col">Comment</th>
                                    <th scope="col">Platform</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                        <div className="row">
                            <div className="col-8">
                                <div className="text-center">
                                    <label>Please select Staff</label>
                                </div>
                                <div className="row">
                                    <div className="col-2">
                                        <label>Last Staff</label>
                                    </div>
                                    <div className="col-10">
                                        <label>Free</label>
                                    </div>
                                    <div className="col-12">
                                        <label>Busy</label>
                                    </div>
                                </div>

                            </div>
                            <div className="col-4">
                                <div className="text-center">
                                    <label>calendar</label>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-8">
                                <div className="text-center">
                                    <label>Please select Staff</label>
                                </div>
                                <div className="row">
                                    <div className="col-9">
                                        <label>Selected Staffs</label>
                                    </div>
                                    <div className="col-3">
                                        <label>Categories</label>
                                    </div>
                                </div>

                            </div>
                            <div className="col-4">
                                <div className="text-center">
                                    <label>Please select service</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer text-center">
                        <button type="button" className="btn btn-primary">Next</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>

                    </div>
                </div>
            </div>
        </div>
    </>


)

export default NavBar;