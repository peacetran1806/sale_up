import { getLayout } from './layouts/SiteLayout'
import UserCard from './Dashboard/UserCard'
import users from './Dashboard/data1'
import { ReactSortable } from "react-sortablejs";
import update from 'immutability-helper';
import { Accordion } from 'react-bootstrap';

import {getCurrentUser} from '../components/auth.service';
import Router from 'next/router';
class Index extends React.Component {
  constructor(props) {
    super(props);
    this.change = this.change.bind(this);
    this.state = {
      InLine: users,
      InService: [],
      CheckOut: []
    }
  }
  componentDidMount() {
    if (getCurrentUser() === null) {
      Router.push("/login");
    }
  }

  change = (event, show, addservice) => {

    //event.preventDefault();
    if (show === 'InLine') {
      const array = this.state.InLine
      const index = array.indexOf(event)
      const a = event
      if (index !== -1) {
        array.splice(index, 1);
        this.setState({
          InService: [...this.state.InService, a],
          InLine: array
        }
        )
      }
    }

    if (show === 'ToService') {
      const array = this.state.InService
      const index = array.indexOf(event)
      const a = event
      if (index !== -1) {
        array.splice(index, 1);
        this.setState({
          CheckOut: [...this.state.CheckOut, a],
          InService: array
        }
        )
      }
    }

    // if (show === 'AddService') {
    //   const data = this.state.InService
    //   const index = data.indexOf(event)
    //   const updatedComment = update(data[index], {servieDesc: {$set: data[index].servieDesc + addservice}});
    //   const newData = update(data, {
    //     $splice: [[index, 1, updatedComment]]
    //   });
    //   this.setState({InService: newData});
    // }
  }


  render() {
    return (
      <div className="content-wrapper" >
        <section className="content-header">
          <h1>
            Dashboard
          <small></small>
          </h1>
        </section>
        <section className="content container-fluid">
          <div className="row">
            <div className="col">
              <Accordion>
                <ReactSortable
                  list={this.state.InLine}
                  setList={newState => this.setState({ InLine: newState })}
                  animation={150}
                  group="shared-group-name"
                >

                  {this.state.InLine.map((user, i) => <UserCard key={i} value={user} change={this.change} Show_Button='InLine' />)}

                </ReactSortable>
              </Accordion>
            </div>
            <div className="col">
              <Accordion>
                <ReactSortable
                  list={this.state.InService}
                  setList={newState => this.setState({ InService: newState })}
                  animation={150}
                  group="shared-group-name"
                >

                  {this.state.InService.map((user, i) => <UserCard key={i} value={user} change={this.change} Show_Button='ToService' />)}

                </ReactSortable>
              </Accordion>
            </div>
            <div className="col">
              <Accordion>
                <ReactSortable
                  list={this.state.CheckOut}
                  setList={newState => this.setState({ CheckOut: newState })}
                  animation={150}
                  group="shared-group-name"
                >

                  {this.state.CheckOut.map((user, i) => <UserCard key={i} value={user} change={this.change} Show_Button='CheckOut' />)}

                </ReactSortable>
              </Accordion>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

Index.getLayout = getLayout

export default Index 
