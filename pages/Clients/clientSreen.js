import ClientForm1 from './ClientForm1'
import ClientForm2 from './ClientForm2'
import ClientForm3 from './ClientForm3'
import ClientCard from './clientCards'
import getCurrentUser from '../../components/auth.service';
import Router from 'next/router';

class Clients extends React.Component {
  constructor(props) {
    super(props);
    this.state = { title: 'Clients' }
  }
  componentDidMount() {
    // if (getCurrentUser() === null) {
    //     Router.push("/login");
    // }
  }
  toggleTitle = (name) => {
    this.setState({ title: name });
  }
  render() {
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <div className="d-flex justify-content-between">
            <div style={{ display: "flex" }}>
              <div className="logo client-logo pull-left" style={{ width: '2.5vw' }}>
                <svg>
                  <line x1="0" y1="100%" x2="100%" y2="0" />
                  <line x1="0" y1="0" x2="100%" y2="100%" />
                </svg>
              </div>
              <h1>{this.state.title}</h1>
            </div>
            <nav>
              <div className="nav nav-tabs justify-content-end" id="nav-tab" role="tablist">
                <a className="nav-item nav-link active" id="nav-clients-tab" data-toggle="tab" href="#nav-clients" role="tab" aria-controls="nav-clients" aria-selected="true" onClick={() => this.toggleTitle('Clients')}>
                  Clients
                </a>
                <a className="nav-item nav-link" id="nav-clientsGroup-tab" data-toggle="tab" href="#nav-clientsGroup" role="tab" aria-controls="nav-clientsGroup" aria-selected="false" onClick={() => this.toggleTitle('New Client Creation Form')}>
                  Clients Group
                </a>
              </div>
            </nav>
          </div>
        </section>
        <section className="content container-fluid">
          <div className="tab-content" id="nav-tabContent">
            <div id="nav-clients" role="tabpanel" aria-labelledby="nav-clients-tab" className="tab-pane fade show active">
              <div className="row">
                <div className="col-10" style={{ display: "flex" }}>
                  <ClientCard />
                </div>
                <div className="col-2" style={{ height: '12.5vw' }}>
                  <div className="form-inline">
                    <div className="pull-right" style={{ position: "absolute", bottom: 0, right: 0 }}>
                      <button type="button" className="btn btn-default btn-lg"> Large button </button>
                      <button type="button" className="btn btn-default btn-lg"> Large button </button>
                    </div>

                  </div>

                </div>
              </div>
              <div className="row">
                <div className="table-responsive">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>
                          <input type="checkbox" value="" />
                        </th>
                        <th>Name</th>
                        <th>Membership ID</th>
                        <th>Birthday</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Last Rating</th>
                        <th>First Signin Date</th>
                        <th>Last Visited</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <input type="checkbox" value="" />
                        </td>
                        <td>ProjectX</td>
                        <td>0123456</td>
                        <td>10/22/1998</td>
                        <td>0123456789</td>
                        <td>abcs@xyx.com</td>
                        <td>012345667</td>
                        <td>01/01/1945</td>
                        <td>19/05/1975</td>
                      </tr>
                      <tr>
                        <td>
                          <input type="checkbox" value="" />
                        </td>
                        <td>ProjectX</td>
                        <td>0123456</td>
                        <td>10/22/1998</td>
                        <td>0123456789</td>
                        <td>abcs@xyx.com</td>
                        <td>012345667</td>
                        <td>01/01/1945</td>
                        <td>19/05/1975</td>
                      </tr>
                      <tr>
                        <td>
                          <input type="checkbox" value="" />
                        </td>
                        <td>ProjectX</td>
                        <td>0123456</td>
                        <td>10/22/1998</td>
                        <td>0123456789</td>
                        <td>abcs@xyx.com</td>
                        <td>012345667</td>
                        <td>01/01/1945</td>
                        <td>19/05/1975</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div id="nav-clientsGroup" role="tabpanel" aria-labelledby="nav-clientsGroup-tab" className="tab-pane fade">
              <div className="mb-3">
                <button className="btn btn-primary mr-3">Create Client</button>
                <button className="btn btn-default">Cancel</button>
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="box box-danger">
                    <ClientForm1 />
                  </div>
                </div>
                <div className="col-6">
                  <div className="box box-danger">
                    <ClientForm2 />
                  </div>
                  <div className="box box-danger">
                    <ClientForm3 />
                  </div>
                </div>
              </div>
              <div>
                <button className="btn btn-primary mr-3">Create Client</button>
                <button className="btn btn-default">Cancel</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default Clients;
