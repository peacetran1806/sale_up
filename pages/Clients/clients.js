export default function ClientScreen() {
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <div className="pull-left" style={{ display: "flex" }}>
          <div className="logo client-logo pull-left">
            <svg>
              <line x1="0" y1="100%" x2="100%" y2="0" />
              <line x1="0" y1="0" x2="100%" y2="100%" />
            </svg>
          </div>
          <h1>Clients</h1>
        </div>
        {/* <div className="pull-right"> */}
          {/* <ul className="nav nav-tabs">
            <li className="active">
              <a data-toggle="tab" href="/#clients">
                Clients
              </a>
            </li>
            <li>
              <a data-toggle="tab" href="/#clientgroup">
                Client Groups
              </a>
            </li>
          </ul> */}
        <nav className="pull-right">
          <div className="nav nav-tabs justify-content-end" id="nav-tab" role="tablist">
            <a className="nav-item nav-link active" id="nav-clients-tab" data-toggle="tab" href="#nav-clients" role="tab" aria-controls="nav-clients" aria-selected="true">Clients</a>
            <a className="nav-item nav-link" id="nav-clientsGroup-tab" data-toggle="tab" href="#nav-clientsGroup" role="tab" aria-controls="nav-clientsGroup" aria-selected="false">Clients Group</a>
          </div>
        </nav>
        {/* </div> */}
      </section>
      <section className="content container-fluid">
        <div className="tab-content" id="nav-tabContent">
          <div id="nav-clients" role="tabpanel" aria-labelledby="nav-clients-tab" className="tab-pane fade show active">
            <div className="col-10" style={{ display: "flex" }}>
              <div className="row">
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">20</span>
                        <p>Visited Customers Today</p>
                      </span>
                    </a>
                  </div>
                </div>
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">20</span>
                        <p>New Cusomer This Month</p>
                      </span>
                    </a>
                  </div>
                </div>
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">15</span>
                        <p>Total Customers</p>
                      </span>
                    </a>
                  </div>
                </div>
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">10</span>
                        <p>Total Free Reward Customers</p>
                      </span>
                    </a>
                  </div>
                </div>
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">50</span>
                        <p>Visited Customers This Weeks</p>
                      </span>
                    </a>
                  </div>
                </div>
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">21</span>
                        <p>Visited Customers Today</p>
                      </span>
                    </a>
                  </div>
                </div>
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">15</span>
                        <p>
                          Visited Free Reward
                          <br />
                          Customers This Week
                        </p>
                      </span>
                    </a>
                  </div>
                </div>
                <div className="col-3 client-col">
                  <div className="client-card">
                    <a href="#" className="card-link">
                      <span className="card-img"></span>
                      <span className="card-body">
                        <span className="h3 heading">25</span>
                        <p>
                          New Free Reward
                          <br />
                          Customers This Week
                        </p>
                      </span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-2" style={{ height: "196px" }}>
              <div className="pull-right" style={{ position: "absolute", bottom: 0, right: 0 }}>
                <button type="button" className="btn btn-default btn-lg">
                  Large button
                </button>
              </div>
            </div>
          </div>
          <div id="nav-clientsGroup" role="tabpanel" aria-labelledby="nav-clientsGroup-tab" className="tab-pane fade">
            <p>
              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="table-responsive">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>
                    <input type="checkbox" value="" />
                  </th>
                  <th>Name</th>
                  <th>Membership ID</th>
                  <th>Birthday</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Last Rating</th>
                  <th>First Signin Date</th>
                  <th>Last Visited</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input type="checkbox" value="" />
                  </td>
                  <td>ProjectX</td>
                  <td>0123456</td>
                  <td>10/22/1998</td>
                  <td>0123456789</td>
                  <td>abcs@xyx.com</td>
                  <td>012345667</td>
                  <td>01/01/1945</td>
                  <td>19/05/1975</td>
                </tr>
                <tr>
                  <td>
                    <input type="checkbox" value="" />
                  </td>
                  <td>ProjectX</td>
                  <td>0123456</td>
                  <td>10/22/1998</td>
                  <td>0123456789</td>
                  <td>abcs@xyx.com</td>
                  <td>012345667</td>
                  <td>01/01/1945</td>
                  <td>19/05/1975</td>
                </tr>
                <tr>
                  <td>
                    <input type="checkbox" value="" />
                  </td>
                  <td>ProjectX</td>
                  <td>0123456</td>
                  <td>10/22/1998</td>
                  <td>0123456789</td>
                  <td>abcs@xyx.com</td>
                  <td>012345667</td>
                  <td>01/01/1945</td>
                  <td>19/05/1975</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </div>
  );
}
