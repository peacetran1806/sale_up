import ClientForm1 from './ClientForm1'
import ClientForm2 from './ClientForm2'
import ClientForm3 from './ClientForm3'
import {getCurrentUser} from '../../components/auth.service';
import Router from 'next/router';

class ClientCreateForm extends React.Component {
    componentDidMount() {
        if (getCurrentUser() === null) {
            Router.push("/login");
        }
    }
    render() {
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        New Client Creation Form
                        <small></small>
                    </h1>
                </section>
                <div className="content container-fluid">
                    <div className="mb-3">
                        <button className="btn btn-primary mr-3">Create Client</button>
                        <button className="btn btn-default">Cancel</button>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="box box-danger">
                                <ClientForm1 />
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="box box-danger">
                                <ClientForm2 />
                            </div>
                            <div className="box box-danger">
                                <ClientForm3 />
                            </div>
                        </div>
                    </div>
                    <div>
                        <button className="btn btn-primary mr-3">Create Client</button>
                        <button className="btn btn-default">Cancel</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ClientCreateForm;