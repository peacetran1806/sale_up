import React, { useState } from 'react';
import Router from 'next/router';
import login  from '../components/auth.service';
import Link from "next/link";

const Login = () => {
  const [loginError, setLoginError] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function handleSubmit(e) {
    e.preventDefault();
    //call api    
    login(email, password).then(
      () => {
        Router.push("/");
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        alert("Login Failed");
      }
    );
  }

  return (
    //<div>
    /* <div className="header">
      <div className="row">
        <div className="col-lg-6">
          <div className="logo">
            <svg>
              <line x1="0" y1="100%" x2="100%" y2="0" />
              <line x1="0" y1="0" x2="100%" y2="100%" />
            </svg>
          </div>
        </div>
        <div className="col-lg-6">
          <label className="signupText" style={{ float: 'right' }}>Already have a SaleUp account?</label>
        </div>
      </div>
    </div> */

    //  <div className="container " style={{borderStyle: 'groove', borderWidth: 3}}>
    //   <div className="left text-center" style={{ left: '50%', transform: 'translateX(-50%) translateY(-50%)', width: '25%' }}>
    //     <h1 style={{ paddingLeft: '1.5em', paddingRight: '1.5em', paddingBottom: '0.5em' }}>LOGIN</h1>
    //     <p className="text-center" style={{ paddingLeft: '4.5em', paddingRight: '4.5em', paddingBottom: '1em' }}>Try shoify for free, and explore all the tools and services you need to start, run, and grow your business</p>
    //     <form action="#" method="post" data-toggle="validator" onSubmit={handleSubmit}>
    //       <div className="form-group">
    //         <input className="form-control"
    //           name="email"
    //           type="text"
    //           value={email}
    //           onChange={(e) => setEmail(e.target.value)}
    //           placeholder="Email address"
    //           required
    //         />
    //       </div>
    //       <div className="form-group">
    //         <input className="form-control"
    //           name="password"
    //           type="password"
    //           value={password}
    //           placeholder="Password"
    //           onChange={(e) => setPassword(e.target.value)}
    //           required
    //         />
    //       </div>
    //       <div className="form-group">
    //         <input type="submit" value="Login" className="btn btn-primary btn-block" />
    //         {loginError && <p style={{ color: 'red' }}>{loginError}</p>}
    //       </div>
    //       <Link href="/store/createstore">
    //         <a>Create Store?</a>
    //       </Link>
    //     </form>
    //   </div>
    //   <div className="clear"></div>
    // </div>
    <div>
      <div className="container " style={{ borderStyle: 'groove', borderWidth: 3, width: '600px', position: 'absolute', top: '50%', left: '50%', marginRight: '-50%', transform: 'translate(-50%,-50%)' }}>
        <br></br>
        <img className="img-fluid" src='./logo.png' style={{ display: 'block', margin: '0 auto' }} />
        {/* marign: 0 auto 0 auto */}
        <p style={{ paddingBottom: '0.5em' }} className="pl-5">Welcome back!</p>
        <h1 style={{ paddingBottom: '0.5em' }} className="pl-5">LOGIN</h1>
        <p style={{ paddingBottom: '0.5em' }} className="pl-5">You don't have an account?
        <Link href="/SignUp/SaleUp_Index">
            <a>Sign up now!</a>
        </Link>
        </p>
        <form action="#" method="post" data-toggle="validator" onSubmit={handleSubmit}>
          <div className="row">
            <div className="col-12">
              <div className="form-group pl-5 pr-5">
                <label>Email address *</label>
                <input size='55' className="form-control" style={{ borderRadius: "7px", height: '50px' }} name="email" type="text" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email address" required />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="form-group pl-5 pr-5">
                <label>Password *</label>
                <input size='55' className="form-control" style={{ borderRadius: "7px", height: '50px', verticalAlign: 'middle' }} name="password" type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Password" required />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 text-right">
              <div className="form-group pr-5">
                <p style={{ fontWeight: 'bold' }}>Forgot your password ?</p>
              </div>
            </div>
          </div>
          <br></br>
          <div className="row">
            <div className='col-4 '>
            </div>
            <div className='col-4 '>
              <input type="submit" value="LOGIN" className="btn btn-block" style={{ backgroundColor: 'rgb(246, 98, 119)', color: 'white' }} />
              {loginError && <p style={{ color: 'red' }}>{loginError}</p>}
            </div>
            <div className='col-4 '>
            </div>
          </div>

        </form>
        <br></br>
        <p className="text-center">Or login by</p>
        <div className='row'>
          <div className='col-4 '>
          </div>
          <div className='col-4 d-flex justify-content-center'>            
            <a className="btn btn-social-icon btn-facebook btn-block" style={{width:'100%'}}><i style={{color:'white'}} className="fa fa-facebook"></i></a>
          </div>
          <div className='col-4'>
          </div>
        </div>
        <br></br>
      </div>
      <div className="clear"></div>
    </div>
  );
};

export default Login;