import React, { Component } from 'react';
import SaleUp_Owner from './SaleUp_Owner';
import SaleUp_Member from './SaleUp_Member';
import Link from "next/link";

const SaleUp_Index = () =>{
    return(
        <div>
            <div className="row">
                <div className="col-lg-6">
                    <div className="logo" style={{paddingLeft:'2em'}}>
                        <img src='../img/logo.png' style={{display:'block', margin:'0 auto', width:'200px', height:'55px'}}/>
                    </div>
                </div>
                 <div className="col-lg-6" style={{paddingRight:'2em'}}>
                     <Link href="/login">
                        <a className="signupText" style={{ float: 'right',fontWeight:'bold',color:'#8B9BAC' }}>Already have a SaleUp account?</a>
                     </Link>
                </div>
            </div>  
            <div className='container'>        
                <br></br>     
                <div className="row" >
                    <div className="container">
                    <div className='col-lg-12'>
                    <ul className="nav nav-pills nav-fill" id="pills-tab" role="tablist" style={{height:'100px'}}>
                                <li className="nav-item " size='100' style={{paddingLeft:'-1%'}}>                              
                                    <a className="nav-link active text-center" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style={{borderRadius: "7px",borderStyle: 'groove',borderColor:'#8B9BAC', height:'60px',borderWidth:2}}>Sign up as a new store owner</a>
                                </li>
                                <li className="nav-item " size='100' style={{paddingLeft:'1%'}}>
                                    <a className="nav-link text-center" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style={{borderRadius: "7px",borderStyle: 'groove',borderColor:'#8B9BAC', height:'60px',borderWidth:2}}>Sing up as a new member of a store</a>
                                </li>
                            </ul>   
                    </div>
                    </div>
                
                </div>

                <div className='row'>
                    <div className='col-lg-6' >
                            <div className="tab-content" id="pills-tabContent" style={{paddingLeft:'3%'}}>
                                <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <SaleUp_Owner/>
                                </div>
                                <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <SaleUp_Member/>
                                </div>
                            </div> 
                    </div>

                    <div className='col-lg-6'>
                            <img src='../img/background.png' style={{display:'block'}} style={{paddingLeft:'20%', width:'522px',height:'701px'}}/>
                    </div>

                </div>         
            </div>
        </div>

    );
}
export default SaleUp_Index;