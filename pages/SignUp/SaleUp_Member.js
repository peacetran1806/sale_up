import React, { useState } from 'react';
import Router from 'next/router';
import cookie from 'js-cookie';
import AuthService from '../../components/auth.service';
import Link from "next/link";
import { Form, Button, Col, Card, InputGroup, FormControl } from 'react-bootstrap';


const Login = () => {
  const [loginError, setLoginError] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function handleSubmit(e) {
    e.preventDefault();
    //call api    
    AuthService.login(email, password).then(
      () => {
        Router.push("/");
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        alert("Login Failed");
      }
    );
  }

  return (
    //<div>
    /* <div className="header">
      <div className="row">
        <div className="col-lg-6">
          <div className="logo">
            <svg>
              <line x1="0" y1="100%" x2="100%" y2="0" />
              <line x1="0" y1="0" x2="100%" y2="100%" />
            </svg>
          </div>
        </div>
        <div className="col-lg-6">
          <label className="signupText" style={{ float: 'right' }}>Already have a SaleUp account?</label>
        </div>
      </div>
    </div> */

    //  <div className="container " style={{borderStyle: 'groove', borderWidth: 3}}>
    //   <div className="left text-center" style={{ left: '50%', transform: 'translateX(-50%) translateY(-50%)', width: '25%' }}>
    //     <h1 style={{ paddingLeft: '1.5em', paddingRight: '1.5em', paddingBottom: '0.5em' }}>LOGIN</h1>
    //     <p className="text-center" style={{ paddingLeft: '4.5em', paddingRight: '4.5em', paddingBottom: '1em' }}>Try shoify for free, and explore all the tools and services you need to start, run, and grow your business</p>
    //     <form action="#" method="post" data-toggle="validator" onSubmit={handleSubmit}>
    //       <div className="form-group">
    //         <input className="form-control"
    //           name="email"
    //           type="text"
    //           value={email}
    //           onChange={(e) => setEmail(e.target.value)}
    //           placeholder="Email address"
    //           required
    //         />
    //       </div>
    //       <div className="form-group">
    //         <input className="form-control"
    //           name="password"
    //           type="password"
    //           value={password}
    //           placeholder="Password"
    //           onChange={(e) => setPassword(e.target.value)}
    //           required
    //         />
    //       </div>
    //       <div className="form-group">
    //         <input type="submit" value="Login" className="btn btn-primary btn-block" />
    //         {loginError && <p style={{ color: 'red' }}>{loginError}</p>}
    //       </div>
    //       <Link href="/store/createstore">
    //         <a>Create Store?</a>
    //       </Link>
    //     </form>
    //   </div>
    //   <div className="clear"></div>
    // </div>
    <div>
      <div className="container " style={{ borderStyle: 'groove', borderWidth: 3, width: '640px', height: '701px' }}>
        <br></br>
        <img src='../img/logo.png' style={{ display: 'block', margin: '0 auto' }} />

        <p style={{ paddingLeft: '2.1em', paddingRight: '1.5em', paddingBottom: '0.5em', color: '#9F9F9F' }}>Hello!</p>
        <p style={{ paddingLeft: '1.5em', paddingRight: '1.5em', marginTop: '-1.3em', color: '#666666', fontWeight: 'bold', fontSize: '1.4em' }}>Are you a member of a SaleUp Store?</p>
        <br></br>
        <form action="#" method="post" data-toggle="validator" onSubmit={handleSubmit}>
          <div className="form-group">
            <label style={{ paddingLeft: '2em', paddingRight: '1.5em', paddingBottom: '0.5em', color: '#666666', fontWeight: 'bold' }}>
              Email address *
              <input size='55' className="form-control" style={{ borderRadius: "7px", height: '60px' }} name="email" type="text" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email address" required />
            </label>

          </div>
          <div className="form-group">
            <label style={{ paddingLeft: '2em', paddingRight: '1.5em', paddingBottom: '0.5em', color: '#666666', fontWeight: 'bold' }}>
              Password *
              <input size='55' className="form-control" style={{ borderRadius: "7px", height: '60px', verticalAlign: 'middle' }} name="password" type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Password" required />
            </label>
          </div>
          <div className='row'>
            <div className="form-check-inline">
              <label className="form-check-label" style={{ paddingLeft: '3em', paddingRight: '1.5em' }}>
                <input type="radio" className="form-check-input" name="optradio" />I accept the policy privacy and terms
              </label>
            </div>
            {/* <i class="fa fa-circle" aria-hidden="true" style={{paddingLeft: '3em', paddingRight: '1.5em'}}></i>
            <p style={{paddingLeft: '0em', paddingRight: '1.5em',color:'#9F9F9F'}}></p> */}
          </div>

          <br></br>
          <div className="row">
            <div className='col-4 '>
            </div>
            <div className='col-4 '>
              <input type="submit" value="SIGN UP" className="btn btn-block" style={{ backgroundColor: 'rgb(246, 98, 119)', color: 'white' }} />
              {loginError && <p style={{ color: 'red' }}>{loginError}</p>}
            </div>
            <div className='col-4 '>
            </div>
          </div>

        </form>
        <br></br>
        <p style={{ textAlign: 'center', color: 'rgb(246, 98, 119)' }}>Or Signup with</p>
        <div className='row'>
          <div className='col-4 '>
          </div>
          <div className='col-4 d-flex justify-content-center'>
            <a className="btn btn-social-icon btn-facebook btn-block" style={{ width: '100%' }}><i style={{ color: 'white' }} className="fa fa-facebook"></i></a>
          </div>
          <div className='col-4'>
          </div>
        </div>
        <br></br>
      </div>
      <div className="clear"></div>
    </div>

  );
};

export default Login;