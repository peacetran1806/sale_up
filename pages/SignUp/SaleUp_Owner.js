import React, { Component } from 'react';
import Router from "next/router"
import createStore  from '../../components/store/store.service'

class CreateStore extends Component{
    constructor(){
        super();
        this.state = {
            emailAddress: '',
            password: '',
            storeName: ''
        };
        this.publish = this.publish.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }
    
    publish(){
        console.log(this.state.emailAddress, this.state.password, this.state.storeName);
    }

    handleSubmit(e){
        e.preventDefault();
        createStore(`mutation createStore($email:String!, $password: String!, $storeName:String!){
            createStore(email:$email, password:$password, storeName:$storeName){
              status
              msg
            }
            }`,{email: this.state.emailAddress, password: this.state.password, storeName: this.state.storeName}). then((response) =>{
                console.log(response);
                if(response.data.createStore.status === true){
                    alert("Created successfully");
                    Router.push("/MyAccount/MyAccount_Index");
                }
                else{
                    alert(response.data.createStore.msg);
                }
            });
            
    }


    submitHandler = e => {
        e.preventDefault();
        axios.post('',this.state);
    }
    render(){
        return (
            <div>
                {/* <div className="header">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="logo">
                                <svg>
                                    <line x1="0" y1="100%" x2="100%" y2="0" />
                                    <line x1="0" y1="0" x2="100%" y2="100%" />
                                </svg>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <label className="signupText" style={{ float: 'right' }}>Already have a SaleUp account?</label>
                        </div>
                    </div>
                </div> */}
                        <div className="container" style={{ width:'640px',height:'701px',backgroundColor:'#F4F6F8'}}>
                            <h1 className='pt-5 pb-1' style={{ textAlign:'center',paddingLeft: '2em', paddingRight: '2em'}}>Start your 90-day free trial today</h1>
                            <br></br>
                            <p className="text-center" style={{ paddingLeft: '0.5em', paddingRight: '1.5em', paddingBottom: '1em',color:'#CBD2DA', fontWeight:'bold', fontSize:'1.2em',color:'#8898AA' }}>Try SaleUp for free, and explore all the tools and services you need to start, run and grow your business. </p>
                            <form action="#" method="post" data-toggle="validator" onSubmit={this.handleSubmit}>
                                <div className="form-group" style={{borderWidth: 3}}>
                                    <input name='emailAddress' style={{borderRadius: "7px", height:'60px', verticalAlign:'middle',backgroundColor:'#F4F6F8'}} type="email" className="form-control" placeholder="Email address"  value={this.state.emailAddress} onChange={this.handleChange} required/>
                                </div>
                                <div className="form-group" style={{paddingTop:'0.3em',borderWidth: 3}}>
                                    <input name='password' style={{borderRadius: "7px", height:'60px', verticalAlign:'middle',backgroundColor:'#F4F6F8'}} type="password" className="form-control"  placeholder="Password"  value={this.state.password} onChange={this.handleChange} required/>
                                </div>
                                <div className="form-group" style={{paddingTop:'0.3em',borderWidth: 3}}>
                                    <input name='storeName' style={{borderRadius: "7px", height:'60px', verticalAlign:'middle',backgroundColor:'#F4F6F8'}} type="text" className="form-control"  placeholder="Your store name"  value={this.state.storeName} onChange={this.handleChange} required/>
                                </div>
                                <div className="form-group" style={{paddingTop:'0.3em'}}>
                                    <button type="submit" className="btn btn-default btn-block"style={{borderRadius: "6px", height:'60px', verticalAlign:'middle', fontWeight:'bold', fontSize:'1.2em',color:'#8898AA',backgroundColor:'#F4F6F8'}} >Create your store</button>
                                </div>
                            </form>
                        </div>
            </div>
        );
    }

}
export default CreateStore;
