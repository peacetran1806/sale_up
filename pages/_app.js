import '../style/bootstrap/css/bootstrap.min.css'
import '../style/font-awesome/css/font-awesome.min.css'
import '../style/Ionicons/css/ionicons.min.css'
import '../style/customize/main.css'
import '../style/customize/ripples.min.css'
import '../style/customize/skinmdredlight.css'
import '../style/customize/createstore.css'
// import '../style/customize/accessedstore.css'
import '../style/customize/clientScreen.css'
import  '../style/customize/registerForm.css';
import React from 'react'
import App from 'next/app'
import SiteLayout from './layouts/SiteLayout'

class MyApp extends App {
  render() {
    const { Component, pageProps, router } = this.props

    const getLayout =
      Component.getLayout || (page => <SiteLayout children={page} />)
    if (router.pathname.startsWith('/SignUp/SaleUp_Member') || router.pathname.startsWith('/login')||router.pathname.startsWith('/SignUp/SaleUp_Owner')||router.pathname.startsWith('/SignUp/SaleUp_Index') ) {
      return (<Component {...pageProps} />)
    }
    return getLayout(<Component {...pageProps} />)
  }
}

export default MyApp