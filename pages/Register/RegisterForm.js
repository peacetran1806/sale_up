import React, { Component } from 'react';
import {Form, Button,Col,Card, InputGroup, FormControl} from 'react-bootstrap';


class MyFrom extends Component{
    render(){
        return(

            <div className="content-wrapper">
                <section className="content-header">
                    <h1>
                        New Client Creation Form
                        <small></small>
                    </h1>
                </section>
                <div className="content container-fluid">
                    <div className="mb-3">
                        <button className="btn btn-primary mr-3">Create Client</button>
                        <button className="btn btn-default">Cancel</button>
                    </div>
                    <div className="container row">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="container shadow p-3 thick" style={{borderRadius: "7px"}}>
                                <Form>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridCRN">
                                            <Form.Label className="font-weight-bold">CRN</Form.Label>
                                            <Form.Control type="email" />
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridExternalID">
                                            <Form.Label className="font-weight-bold">External ID</Form.Label>
                                            <Form.Control type="email" />
                                        </Form.Group>
                                    </Form.Row>

                                    <Form.Row >
                                        <Form.Group as={Col} controlId="formGridClientName">
                                            <Form.Label className="font-weight-bold">Client Name</Form.Label>
                                            <Form.Control as="select" value ="--- Title ---">
                                                <option>Choose...</option>
                                                <option>...</option>
                                            </Form.Control>
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridClientNameName">
                                            <Form.Label>&shy;</Form.Label>
                                            <Form.Control placeholder="First name" />
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridClientSurname">
                                            <Form.Label>&shy;</Form.Label>
                                            <Form.Control placeholder="Surname" />
                                        </Form.Group>
                                    </Form.Row>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridDateOfBirth">
                                            <Form.Label className="font-weight-bold">DOB</Form.Label>
                                            <Form.Control as="select" value ="01/01/2010">
                                                <option>Choose...</option>
                                                <option>...</option>
                                            </Form.Control>
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridGender">
                                            <Form.Label className="font-weight-bold">Gender</Form.Label>
                                                <Form.Control as="select">
                                                    <option>Choose...</option>
                                                    <option>...</option>
                                                </Form.Control>
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridClientSurname">
                                            <Form.Label className="font-weight-bold">Client Group</Form.Label>
                                            <InputGroup className="mb-3">
                                                <FormControl  as="select" aria-describedby="basic-addon2">
                                                    <option>General</option>
                                                    <option>...</option>
                                                </FormControl>
                                                <InputGroup.Append>
                                                    <Button variant="outline-secondary" className="font-weight-bold">Edit</Button>
                                                </InputGroup.Append>
                                            </InputGroup>                              
                                        </Form.Group>

                                    </Form.Row>   

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridCRN">
                                            <Form.Label className="font-weight-bold">CRN</Form.Label>
                                            <Form.Control type="email" />
                                        </Form.Group>
                                    </Form.Row>        
                                    <hr></hr>
                                    <br></br>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridResidentalAddress">
                                            <Form.Label className="font-weight-bold">Residental Address</Form.Label>
                                            <Form.Control type="email" placeholder="Address" />
                                        </Form.Group>
                                    </Form.Row>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridSubrb" className="col-lg-9">
                                            <Form.Control type="email" placeholder="Subbrb" />
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridSubrb" className="col-lg-3">
                                            <Form.Control type="email" placeholder="Postcode" />
                                        </Form.Group>
                                    </Form.Row>  

                                    <br></br>
                                    <br></br>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridSPostalAddress">
                                            <Form.Label className="font-weight-bold">Postal Address</Form.Label>
                                            <Form.Control type="email" placeholder="Address" />
                                        </Form.Group>                                               
                                    </Form.Row>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridSubrb2" className="col-lg-9">
                                            <Form.Control type="email" placeholder="Subbrb" />
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridSubrb2" className="col-lg-3">
                                            <Form.Control type="email" placeholder="Postcode" />
                                        </Form.Group>                               
                                    </Form.Row>

                                    <hr></hr>
                                    <br></br>
                                    <br></br>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridMobile" className="col-lg-6">
                                            <Form.Label className="font-weight-bold">Contact Numbers</Form.Label>
                                            <Form.Control type="email" placeholder="Mobile" />
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridHome" className="col-lg-6">
                                            <Form.Label>&shy;</Form.Label>
                                            <Form.Control type="email" placeholder="Home" />
                                        </Form.Group>
                                    </Form.Row> 

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridHome" className="col-lg-6">
                                                <Form.Control type="email" placeholder="Work" />
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridHome" className="col-lg-6">
                                            <Form.Control type="email" placeholder="Fax" />
                                        </Form.Group>
                                    </Form.Row> 

                                </Form>
                            </div>    
                    </div>
                            <div className="col-lg-6">
                                <div className="container thick shadow p-3" style={{borderRadius: "7px"}}>
                                    <Form.Row>
                                        <Form.Label as={Col} className="col-lg-6" className="font-weight-bold">History / Membership</Form.Label>
                                        <Button variant="primary" className="font-weight-bold">Manage History</Button>
                                    </Form.Row>
                                    <hr></hr>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridMembershipId" className="col-lg-6">
                                            <Form.Label className="font-weight-bold">Membership Id</Form.Label>
                                            <Form.Control type="email"/>
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridMembershipCard" className="col-lg-6">
                                            <Form.Label className="font-weight-bold">Memberhsip card</Form.Label>
                                            <InputGroup className="mb-3">
                                                <FormControl/>
                                                <InputGroup.Append>
                                                    <InputGroup.Text Style="background-color:white">/</InputGroup.Text>
                                                    <InputGroup.Text Style="background-color:white"></InputGroup.Text>
                                                </InputGroup.Append>
                                            </InputGroup>
                                        </Form.Group>                               
                                    </Form.Row>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridMembershipLevel" className="col-lg-6">
                                            <Form.Label className="font-weight-bold">Membership Level</Form.Label>
                                            <InputGroup className="mb-3">
                                                <FormControl  as="select" aria-describedby="basic-addon2">
                                                    <option></option>
                                                </FormControl>
                                                <InputGroup.Append>
                                                    <Button variant="outline-secondary" className="font-weight-bold">Edit</Button>
                                                </InputGroup.Append>
                                            </InputGroup> 
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridMembershipStartDate" className="col-lg-6">
                                            <Form.Label className="font-weight-bold">Membership start date</Form.Label>
                                            <InputGroup className="mb-3">
                                                <FormControl/>
                                                <InputGroup.Append>
                                                    <Button variant="outline-secondary" className="font-weight-bold">x</Button>
                                                </InputGroup.Append>
                                            </InputGroup>
                                        </Form.Group>                               
                                    </Form.Row>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridCurrentRewardPoint" className="col-lg-6">
                                                <Form.Label className="font-weight-bold">Current reward point</Form.Label>
                                                <Form.Control type="email"/>
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridRewardEarnedVal" className="col-lg-6">
                                            <Form.Label className="font-weight-bold">Reward earned value</Form.Label>
                                            <Form.Control type="email" />
                                        </Form.Group>
                                    </Form.Row>

                                    <div className="row">
                                        <div className="col-lg-6">
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridLastReview">
                                                        <Form.Label className="font-weight-bold">Last Review</Form.Label>
                                                        <FormControl  as="select" aria-describedby="basic-addon2">
                                                            <option></option>
                                                        </FormControl>
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridLastStaff">
                                                        <Form.Label className="font-weight-bold">Last Staff</Form.Label>
                                                        <FormControl  as="select" aria-describedby="basic-addon2">
                                                            <option></option>
                                                        </FormControl>
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridLastVisitedDate">
                                                    <Form.Label className="font-weight-bold">Last visisted date</Form.Label>
                                                    <InputGroup className="mb-3">
                                                        <FormControl as="select" aria-describedby="basic-addon2"/>
                                                            <InputGroup.Append>
                                                                <Button variant="outline-secondary" className="font-weight-bold">x</Button>
                                                            </InputGroup.Append>
                                                    </InputGroup>
                                                </Form.Group>
                                            </Form.Row>
                                        </div>
                                        <div className="col-lg-6">
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridRewardEarnedVal">
                                                    <Form.Label className="font-weight-bold">Note</Form.Label>
                                                    <Form.Control as="textarea" rows="8" />
                                                </Form.Group>
                                            </Form.Row>
                                        </div>
                                    </div>
                                </div>
                                <br></br>
                                <div className="container thick shadow p-3" style={{borderRadius: "7px"}}>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridSite">
                                            <Form.Label className="font-weight-bold">Site</Form.Label>
                                                <FormControl  as="select" aria-describedby="basic-addon2">
                                                    <option></option>
                                                </FormControl>
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridDisclosure">
                                            <Form.Label className="font-weight-bold">Disclosre</Form.Label>
                                                <FormControl  as="select" aria-describedby="basic-addon2">
                                                    <option></option>
                                                </FormControl>
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridCaseStatus">
                                                <Form.Label className="font-weight-bold">Case Status</Form.Label>
                                                <Form.Control type="email" placeholder="Current" />
                                        </Form.Group>
                                        <Form.Group as={Col} controlId="formGridLastVisitedDate">
                                            <Form.Label className="font-weight-bold">Market Source</Form.Label>
                                            <InputGroup className="mb-3">
                                                <FormControl as="select" aria-describedby="basic-addon2"/>
                                                <InputGroup.Append>
                                                    <Button variant="outline-secondary" className="font-weight-bold">Edit</Button>
                                                </InputGroup.Append>
                                            </InputGroup>
                                        </Form.Group>                               
                                    </Form.Row>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridMarketingConsent">
                                            <Form.Label className="font-weight-bold">Marketing Consent</Form.Label>
                                            <FormControl as="select" aria-describedby="basic-addon2" placeholder="Marketing- Consent Unknown"/>
                                        </Form.Group>
                                    </Form.Row> 

                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button className="btn btn-primary mr-3">Create Client</button>
                        <button className="btn btn-default">Cancel</button>
                    </div>
                </div>
            </div>

        );

    };
}
export default MyFrom ;