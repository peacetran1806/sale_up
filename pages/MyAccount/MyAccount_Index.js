import MyAccount from './MyAccount_Account'
import StoreInformation from './MyAccount_StoreInformation'
import OpenHour from './MyAccount_OpenHours'
import AuthService from '../../components/auth.service';
import Router from 'next/router';

class MyAccount_Index extends React.Component {
    // componentDidMount() {
    //     if (AuthService.getCurrentUser() === null) {
    //         Router.push("/login");
    //     }
    // }
    render() {
        return (
            <div className="content-wrapper" >
                <section className="content-header">
                    <h1>
                        My Account
                <small></small>
                    </h1>
                </section>
                <section className="content container-fluid">
                    <MyAccount />
                    <StoreInformation />
                    <OpenHour />
                </section>
            </div>
        )
    }
}

export default MyAccount_Index