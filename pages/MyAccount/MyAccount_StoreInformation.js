const MyAccount_StoreInformation = () => {
    return (
        <div className="row">
            <div className="col-md-5">
                <h4>Store Information</h4>
            </div>
            <div className="col-md-6">
                <div className="box box-danger">
                    <form role="form">
                        <div className="box-body">
                            <div className="form-group">
                                <label>Store Name</label>
                                <input className="form-control" placeholder="" />
                            </div>
                            <div className="form-group">
                                <label>Store Address</label>
                                <input className="form-control" placeholder="" />
                            </div>
                            <div className="form-group">
                                <label>Phone Number</label>
                                <div className="input-group mb-3">
                                    <input type="text" className="form-control" placeholder="" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                    <div className="input-group-append">
                                        <span className="input-group-text" id="basic-addon2">X</span>
                                    </div>
                                </div>
                                <a href="#" style={{ color: 'blue' }}>Add a new phone number</a>
                            </div>
                            <div className="form-group">
                                <label>Store Website</label>
                                <input className="form-control" placeholder="" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    )
}

export default MyAccount_StoreInformation 