import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <html>
        <Head>
          <script src="/static/jquery/dist/jquery.min.js"></script>
          <script src="/static/bootstrap/js/bootstrap.min.js"></script>
          <script src="/static/main.js"></script>
        </Head>
        <body className="hold-transition skin-red-light sidebar-mini">
          <Main />
          <NextScript />
        </body>

      </html>
    )
  }
}

export default MyDocument