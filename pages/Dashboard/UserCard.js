/* eslint-disable react/jsx-pascal-case */
import React from 'react';
import StarRatings from 'react-star-ratings'; 
import {Accordion, Card, Row, Col, Modal, Button } from 'react-bootstrap';
//import {   } from "@fortawesome/free-solid-svg-icons";
//import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
//import Update from '@material-ui/icons/Update';

export default class UserCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_Modal: false, 
            show_ToService: false
        };
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleShowModal = this.handleShowModal.bind(this);
        this.Button_Change = this.Button_Change.bind(this);
        // this.handelService = this.handelService.bind(this);
        // this.handleChange = this.handleChange.bind(this);
        this.handleProp = this.handleProp.bind(this);
        //console.log(this.props);
    }

    componentWillReceiveProps(nextProps){
        this.handleProp(nextProps);
    }
    handleProp(props){
        return props.value;
    }
    
    handleCloseModal = () => {this.setState({show_Modal: false})}
    handleShowModal = () => {this.setState({show_Modal: true})}
    
    handleCloseAddServiceModal = () => {this.setState({show_ToService: false})}
    handleShowAddServiceModal = () => {this.setState({show_ToService: true})}

    // handelService = (event) => {
    //     event.preventDefault();
    //     this.props.change(this.props.value, 'AddService',this.state.service);
    // }


    // handleChange(event) {
    //     this.setState({service: event.target.value});
    // }

    Button_Change = () => {
        if (this.props.Show_Button === 'InLine')
        {
            return (
                <button className = "btn btn-primary" style = {{margin: "0px" ,display: "block", width: "100%"}} variant="secondary" onClick= {()=> {this.props.change(this.props.value, this.props.Show_Button, [])}} >
                    To Service
                </button>
            )
        }
        else{
            if (this.props.Show_Button === 'ToService') {
                return (
                    <Row>
                        <Col>
                            <Button style = {{margin: "0px" ,display: "block", width: "100%"}} variant="secondary" onClick= {()=> this.handleShowAddServiceModal()} >
                                Add Service
                            </Button>
                        </Col>
                        <Col>
                            <Button style = {{margin: "0px" ,display: "block", width: "100%"}} variant="secondary" onClick= {()=> {this.props.change(this.props.value, this.props.Show_Button,[])}} >
                                Check Out
                            </Button>
                        </Col>
                    </Row>
                )
            }
            else
            {
                return(
                    <>
                         <Button style = {{margin: "0px" ,display: "block", width: "100%"}} variant="secondary" onClick= {() => this.handleShowModal()} >
                            View Full
                        </Button>
                    </>
                )
            }
    
        }
    }
    // Button_Change = () => {
    //     if (this.props.Show_Button === 'InLine')
    //     {
    //         return (
    //             <Button variant="secondary" onClick= {()=> {this.props.change(this.props.value)}} >
    //                 To Service
    //             </Button>
    //         )
    //     }
    // }
    render() {
        //const value = this.props.value;
        return (
            <>
                <Card 
                    className = '' 
                    onDoubleClick = {this.handleShowModal} 
                    style={ { backgroundColor: '#fff', marginTop: "10px"}}
                > 
                    <Accordion.Toggle as={Card.Body} >
                        <Card.Body className="d-flex flex-column">      
                            <Card.Text className="text-secondary">
                                <Row style={{ marginBottom: "5px"}}>
                                    <Col className="col-lg-8" style={{fontSize:"10pt", paddingTop: "0px"}}>
                                        <div className="form-inline">
                                            <i className="fa fa-clock-o" aria-hidden="true"></i>  
                                            <label className="fontNormal"></label>
                                            &nbsp;&nbsp;&nbsp;
                                            {/* <i className="fa fa-handshake-o"  aria-hidden="true" style={{fontSize:"12pt"}}></i> */}
                                            {/* <Update/> */}
                                            
                                            <label className="fontNormal"></label>
                                        </div>                           
                                    </Col>
                                    <Col className="col-lg-4 ">
                                        <div className="form-row">
                                            <div className="col-lg-10">
                                                {/* <button className={`btn ${this.props.value.status == 'Ready' && this.props.value.category == 'Checkin' ? "btn-success" : 
                                                this.props.value.status == 'Booked' && this.props.value.category == 'Checkin' ? "btn-pink" : 
                                                this.props.value.status == 'In service' && this.props.value.category == 'InService' ? "btn-primary" :
                                                this.props.value.status == 'Waiting' && this.props.value.category == 'Checkin'? "btn-warning" : 
                                                this.props.value.status == 'Checked-out' && this.props.value.category == 'Waiting4inv' ? "btn-gray" : "btn-info"} btn-block`}
                                                style={{fontSize:"10pt", paddingTop: "0px"}}>{this.props.value.status}</button> */}

                                            </div>
                                            <div className="col-lg-2">
                                                <button className="btn btn-default" style={{fontSize:"10pt", paddingTop: "0px"}}>...</button>
                                            </div>                                
                                        </div>                            
                                    </Col>
                                </Row>
                                <Row className="row pb-3">
                                    <Col className="col-lg-4">
                                        <i className="fa fa-user" style={{fontSize:"10pt"}} aria-hidden="true"></i>
                                        <label style={{fontSize:"10pt"}}  className="fontNormal"></label>  
                                    </Col>
                                    
                                    <Col  className="col-lg-4">
                                        <div className="form-inline">
                                        <i className="fa fa-pencil-square-o fontNormal" aria-hidden="true" style={{paddingTop:"0px", fontSize:"10pt" }}></i>
                                        <StarRatings
                                            //rating={this.props.value.rating}
                                            starDimension="8px"
                                            starSpacing="5px"    
                                            starRatedColor = "orange"    
               
                                        /> 
                                        </div>                       
                                    </Col>
                                    <Col className="col-lg-4" style={{letterSpacing:"0.13rem"}}>
                                        {/* {this.props.value.category == 'Checkin' ?  <i className="fa fa-phone" style={{fontSize:"8pt"}} aria-hidden="true"></i> : ''}
                                            <label className="fontNormal" style={{fontSize:"10pt"}}>{this.props.value.category == 'Checkin' ? this.props.value.telNumber  : 
                                                                            this.props.value.category == 'Waiting4inv' ? this.props.value.review : this.props.value.servieDesc}</label> */}


                                    </Col>
                                </Row>
                            </Card.Text>
                        </Card.Body>
                    </Accordion.Toggle>
                    <Accordion.Collapse >
                        <Card.Body>
                            <div className="row pb-3">
                                <div className="col-lg-8">
                                    <i className="fa fa-book" aria-hidden="true"></i>
                                    <label className="fontNormal">
                                    {/* {this.props.value.servieDesc} */}
                                    </label>
                                </div>
                                <div className="col-lg-4">
                                    <i className="fa fa-user-circle" aria-hidden="true"></i>
                                    <label  className="fontNormal" style={{fontSize:"15pt"}}>
                                    {/* {this.props.value.staffName} */}
                                    </label>
                                </div>
                            </div>
                            <Row>
                                <Col className="col-lg-2"></Col>
                                <Col className="col-lg-8">
                                    <this.Button_Change />
                                </Col>
                                <Col className="col-lg-2"></Col>
                            </Row>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
                
                <Modal show={this.state.show_Modal} onHide={this.handleCloseModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Information of User</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                            <p> Id        :  </p>
                    </Modal.Body>
                    
                    <Modal.Footer>
                        <Button variant="secondary" onClick={()=>this.handleCloseModal(this.props)}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show_ToService} onHide={this.handleCloseAddServiceModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form className = 'Add_Client' action="" >
                            <p> Id        :  </p>

                            <input 
                                type="text" 
                                placeholder="service"
                                name = "service"
                                required/>
                            <button className = 'Add_Card'>Add card</button>
                        </form>
                    </Modal.Body>
                    
                    <Modal.Footer>
                        <Button variant="secondary" onClick={()=>this.handleCloseAddServiceModal(this.props)}>
                            Closvasvasvase
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        )
    }
}

